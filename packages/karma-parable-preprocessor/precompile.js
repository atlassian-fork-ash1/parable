#!/usr/bin/env node
'use strict';

const glob = require('glob');
const path = require('path');
const parable = require('parable');

let karmaConfigPath = path.resolve(process.cwd() + '/' + (process.argv[2] || 'karma.conf.js'));
const karmaConfig = require(karmaConfigPath);
let config = {};

karmaConfig({
    set: (value) => config = value
});

// find everything that is to be preprocessed by Parable
const specs = Object.keys(config.preprocessors)
    .filter(fileSpec =>
        config.preprocessors[fileSpec].find(preprocessor => preprocessor === 'parable')
    );

const files = specs.reduce(
        (acc, spec) => acc.concat(
            glob.sync(spec, {
                cwd: config.basePath
            })
        ), [])
    .map(file => {
        if (!path.isAbsolute(file)) {
            return path.join(config.basePath, file);
        } else {
            return file;
        }
    });

console.log(`Starting precompile of ${files.length} files`);
const start = Date.now();
parable.transformFiles(files, config.parablePreprocessor)
    .then(files => console.log(`Pre-compiled ${files.length} files in ${(Date.now() - start)} ms`))
    .catch(errs => {
        errs.forEach(err => {
            console.error(err.message);
            if (err.detail) {
                console.error(err.detail);
            }
        });
        process.exit(1);
    });
