const _ = require('lodash');
const applySourceMap = require('vinyl-sourcemaps-apply');
const fs = require('fs');
const glob = require('glob');
const gulpUtil = require('gulp-util');
const parable = require('parable');
const replaceExt = require('replace-ext');
const through = require('through2');

function logError(error) {
    try {
        if (error instanceof parable.ParableError) {
            gulpUtil.log(gulpUtil.colors.bgRed('JS Error') + ' ' + gulpUtil.colors.reset(error.message)
                + '\n' + gulpUtil.colors.reset(error.detail));
        } else {
            gulpUtil.log(gulpUtil.colors.red(error));
        }
    } catch (err) {
        console.error('Error in error handler!', err);
    }

}

function precompile(globs, options) {
    return function (callback) {
       if (!Array.isArray(globs)) {
            globs = [globs];
        }
        const files = _.flatMap(globs, pattern => {
            let globOptions;

            if (typeof pattern === 'object') {
                globOptions = pattern.options;
                pattern = pattern.pattern;
            }

            return glob.sync(pattern, globOptions);
        });

        gulpUtil.log(`Starting precompile of ${files.length} files`);
        const start = Date.now();
        parable.transformFiles(files, options)
            .then(files => {
                gulpUtil.log(`Pre-compiled ${files.length} files in ${(Date.now() - start)} ms`);
                callback();
            })
            .catch(errors => {
                errors.forEach(error => logError(error));
                callback(new Error('Precompile failed with ' + errors.length + ' errors'));
            });
    }
}

function transform(parableOpts) {
    return through.obj(function (file, enc, cb) {
        parable.transpile(file.path, parableOpts)
            .then(transpiled => {
                const mapFile = file.clone();
                file.contents = new Buffer(transpiled.code);
                file.path = replaceExt(file.path, '.js');
                this.push(file);

                if (transpiled.map) {
                    const sourceMap = transpiled.map;

                    // based on similar fix done for gulp-babel-minify
                    // https://github.com/babel/minify/issues/862
                    if (!sourceMap.hasOwnProperty('file')) {
                        sourceMap.file = file.path;
                    }

                    sourceMap.file = replaceExt(sourceMap.file, '.js');
                    applySourceMap(file, sourceMap);
                    mapFile.path = mapFile.path + '.map';
                    mapFile.contents = new Buffer(JSON.stringify(file.sourceMap));
                    this.push(mapFile);
                }
                cb();
            }).catch(err => {
                cb(err);
            });
    });
}

module.exports = {
    logError,
    precompile,
    transform
};