function ParableError(error, filename) {
    if (error.stack) {
        this.message = error.stack.split('\n')[0];
    } else {
        this.message = error.message;
    }

    if (error._babel) {
        this.detail = error.codeFrame;
    } else if (error.detail) {
        this.detail = error.detail;
    }

    if (filename) {
        this.filename = filename;
    }

    this.originalError = error;
}

ParableError.prototype = Object.create(Error.prototype);
ParableError.prototype.constructor = ParableError;

module.exports = ParableError;