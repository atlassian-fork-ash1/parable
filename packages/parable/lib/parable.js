'use strict';

const cluster = require('cluster');
const fs = require('fs-extra');
const path = require('path');
const tmp = require('tmp');
const winston = require('winston');

const config = require('./config');
const master = require('./master');
const ParableError = require('./error');
const processor = require('./processor');

function transformFiles(files, options) {
    if (cluster.isMaster) {
        return master(files, options);
    } else {
        throw new Error(`Cluster worker should not call parable.transformFiles!`);
    }
}

function transpile(source, options) {
    if (options && !options.skipOutputDirCheck && options.output === config.Output.FILE) {
        fs.mkdirpSync(options.outputDir);
    }

    if (options && options.base && !Array.isArray(options.base)) {
        options.base = [options.base];
    }

    return processor.transpile(source, options);
}

module.exports = {
    Output: config.Output,
    ParableError,
    transformFiles,
    transpile,
    _processor: processor,
};